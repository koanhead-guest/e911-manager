<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
//require_once('root.php');
//require_once "resources/require.php";
//require_once('resources/header.php');
require_once('tests/z.php');

$page = docbase();

$output_div = $page->getElementById('modules_area');

$modules = file( 'module_list.txt', FILE_USE_INCLUDE_PATH | FILE_IGNORE_NEW_LINES);
$module_list = $page->createElement('ul', '');

$module_list_items = array2Fragment($modules, 'li', $page);

//foreach ($module_list_items->childNodes as $elem);
/* this only affected the last item in the nodeList
 * per https://secure.php.net/manual/en/class.domnodelist.php: 
 * "You can modify, and even delete, nodes from a DOMNodeList if you iterate backwards"
 * WTFever
*/ 

for ($i = $module_list_items->childNodes->length; --$i >= 0;) {
    $iter_obj = $module_list_items->childNodes->item($i);
    $iter_obj->setAttribute("class", "api_module");
    $iter_obj->setAttribute("onclick", "showme_the('Request', '?module="
         . $iter_obj->nodeValue 
         . "', 'methods_area')");
    }
$module_list->appendChild($module_list_items);
$output_div->appendChild($module_list);

print($page->saveHTML());
