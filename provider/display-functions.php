<?php
/*
 * display-functions.php
 * 
 * Copyright 2016 koanhead <koanhead@callpipe.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
function docbase($doc=BASEDOC) {
    $page = new DOMDocument;
    $page->loadHTMLFile($doc, LIBXML_NOENT);
    return $page;
}

function script_tag($filename) {
	//constructs script tag(s). $filename is script file to include, one at a time pls
	return(NULL);
}
	
//might not need this bc (array) typecasting
function objectKeys2Array($object, $key_name) {
	$keys = Array();
for ($i=0; $i < count($object); $i++) {
	array_push($keys, $object[$i]->$key_name);
	}
return $keys;
}

function variable2Element($var, $tag_name, $parent) {
	$element = $parent->createElement($tag_name, $var);
	return $element;
}
function xmlPreamble($styleFile){
	$xml_decl = '<?xml version="1.0" encoding="UTF-8"?>'. "\n";
	$xml_stylesheet = '<?xml-stylesheet type="text/xsl" href=' . '"' . $styleFile .'"' . '?>' . "\n";
	if ($styleFile != NULL) {
	    $out = $xml_decl . $xml_stylesheet;
	}
	else $out = $xml_decl;
	
	return($out);
}

function php2XML($phpObject) {
	// initialize XML_Serializer
	include_once('serializer-options.php');
	$xmler = new XML_Serializer($serializer_options);
	// serialize object to XML
	$xml_input = $xmler->serialize($phpObject);
	if ($xml_input===TRUE) {
		$xml_data = $xmler->getSerializedData();
	}
	//header('Content-type: application/xml');
	// add this b4 functin invocation
	$out =  $xml_data;
	return($out);
}

function addXSLTemplate($styleFile, $path) {
	//open $styleFile
	$style_doc = basedoc($styleFile);
	//look for XPath
	$x_path_list = $styleDoc->getElementsByTagName("xsl:for-each");
	if( !in_array($path, $x_path_list)) {
			//if it isnt there add it
		}	
		return;	
}
// $funcs_list = $document->createElement('div', $xml_data);
