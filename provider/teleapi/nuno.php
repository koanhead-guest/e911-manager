<?php
/*
 * controls.php
 * 
 * Copyright 2017 koanhead <koanhead@fagioli>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
require_once('/var/www/fusionpbx/app/e911-manager/tests/z.php');

$document = docbase();

$controllers_container = $document->getElementById("controllers_area");
$methods_container = $document->getElementById("methods_area");

// provisional, will remove after parameterization
$controller = 'TeleCustomers';
require_once('Token.php');
$instance = new $controller($token);
$intron = new ReflectionClass($instance);
$funcs = $intron->getMethods();
//var_dump($funcs);

$controller_header = $document->createElement('h2', $controller);
$methods_container->appendChild($controller_header);

//array2Fragment($test_array, 'li', $document, 'ul');

//extract($test_array);
	


print($document->saveHTML());

?>

