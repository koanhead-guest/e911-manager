# README for PHP Class Library Web Viewer

## Purpose

    This software intends to provide an easy way to generate a Web page from a PHP class library.
    The Web page produced contains a list of subclasses. Clicking on a subclass produces a table of methods and one of properties.
    For each property, the related table will enumerate its value(s).
    For each method, the related table will contain input field(s) for parameter value entry, and a button to call the method.
    
    The software accepts "filters" which limit the methods and properties displayed, as well as the parameters thereof, so that the software can be used to generate an interface suitable for end-users.
    
## Dependencies

    This is a PHP program. It was developed under PHP5. It uses ReflectionClass and DOMDocument, both of which are included in most PHP distributions.
    The only other dependency is the PHP class library you want it to work on.

## Vendor-specific
	This bit should live in a separate file, we'll make that happen later.
	teleapi has a confusing collection of names in their data structure. For the purpose of this adapter, "Controllers" are the top-level elements of the API, reflected as subclasses of BaseTele in client.php
	"Modules" are the methods of Controllers, and parameters are parameters?
Classname
	Properties
		Name
		Value
	Methods
		Name
		Params (button if empty, list of <input> w/button otherwise) where button calls call()
