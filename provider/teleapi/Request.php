<?php
/*
 * teleapiRequest.php
 * 
 * Copyright 2016 koanhead <koanhead@callpipe.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
require_once('/var/www/fusionpbx/app/e911-manager/includes/bootstrap.php');
require_once('Token.php');
print_r($_GET);
$request = $_GET;
/*
if (!isset($request)) {
    echo ("Superglobal $__GET is not set\n");
    * https://secure.php.net/manual/en/function.isset.php 
    * Only works with variables so probably not with $__GET
}

else {
   echo "GET is " . var_dump($request) ."\n";
}*/
//echo "GET is a ". whatis($request)."\n";
echo "GET is " . print_r($request) ." according to print_r\n";
echo "GET is " . var_dump($request) ." according to var_dump\n";
echo $request['module']." will be assigned to called_module. \n";

$called_module = $request["module"];
/*
if (!in_array($called_module, $modules)) {
    die("Called $called_module is not on the list\n");    
}
*/


$module = new $called_module($token);
$functions = get_class_methods(get_class($module));
//require_once('display-functions.php');
//$parent = docbase();
//$output = listify($functions, $parent);
//print($parent->saveHTML($output));


if (isset($__GET['function'])) {
    $called_function = $__GET['function'];
    if (!in_array($called_function, $functions)) {
        die("$function is not a valid method on $called_module\n");
    }

    //do something with $module->function();
}

function get_subclasses($baseclass='BaseTele'){
    /* this does not really do what it says
    *  that would be more like 
    *  fopen($listing), grep "extends $baseclass"
    *  but this is quick n dirty
    * TODO fix this by grep -R modules/ | cut -d ' ' -f2
    * done in glommodules.sh in modules/
    * TODO implement above in PHP, replacing this function
    */
    
    $listing = array_map(function($elem){
        return(explode('.', $elem)[0]);
        }
     , preg_grep('*.php$*', scandir(BASECLASS_PATH.'modules/')));
     
    return($listing);    
}
// valid requests will be of form "module, function, params"
// get object corresponding to request
// we could use reflection to validate the function names & params
// but it might not work

$function = $request['function'];
$params = $request['params'];

if (!is_string($params)) {
	$default = "list_customers"; //.$called_module;
	$object = $module->$function();
	// this fails bc list_ functions are not named sanely
}
else {
	$object = $module->$function($params);
}

$serial = new XML_Serializer($serializer_options);
$useless_varname = $serial->serialize(json_decode($object));
$xml_output = $serial->getSerializedData();
print($xml_output);

?>
