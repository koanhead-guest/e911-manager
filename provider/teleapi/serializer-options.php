<?php

$serializer_options = array(
/**
 * option: string used for indentation
 *
 * Possible values:
 * - any string (default is any string)
 */
//XML_SERIALIZER_OPTION_INDENT => indent, 
'XML_SERIALIZER_OPTION_INDENT' => '    ',

/**
 * option: string used for linebreaks
 *
 * Possible values:
 * - any string (default is \n)
 */
//XML_SERIALIZER_OPTION_LINEBREAKS => linebreak, 

/**
 * option: enable type hints
 *
 * Possible values:
 * - true
 * - false
 */
//XML_SERIALIZER_OPTION_TYPEHINTS => typeHints, 

/**
 * option: add an XML declaration
 *
 * Possible values:
 * - true
 * - false
 */
'XML_SERIALIZER_OPTION_XML_DECL_ENABLED' => false,

/**
 * option: encoding of the document
 *
 * Possible values:
 * - any valid encoding
 * - null (default)
 */
//XML_SERIALIZER_OPTION_XML_ENCODING => encoding, 

/**
 * option: default name for tags
 *
 * Possible values:
 * - any string (XML_Serializer_Tag is default)
 */
//XML_SERIALIZER_OPTION_DEFAULT_TAG => defaultTagName, 

/**
 * option: use classname for objects in indexed arrays
 *
 * Possible values:
 * - true
 * - false (default)
 */
//XML_SERIALIZER_OPTION_CLASSNAME_AS_TAGNAME => classAsTagName, 

/**
 * option: attribute where original key is stored
 *
 * Possible values:
 * - any string (default is _originalKey)
 */
//XML_SERIALIZER_OPTION_ATTRIBUTE_KEY => keyAttribute, 

/**
 * option: attribute for type (only if typeHints => true)
 *
 * Possible values:
 * - any string (default is _type)
 */
//XML_SERIALIZER_OPTION_ATTRIBUTE_TYPE => typeAttribute, 

/**
 * option: attribute for class (only if typeHints => true)
 *
 * Possible values:
 * - any string (default is _class)
 */
//XML_SERIALIZER_OPTION_ATTRIBUTE_CLASS => classAttribute, 

/**
 * option: scalar values (strings, ints,..) will be serialized as attribute
 *
 * Possible values:
 * - true
 * - false (default)
 * - array which sets this option on a per-tag basis
 */
//XML_SERIALIZER_OPTION_SCALAR_AS_ATTRIBUTES => scalarAsAttributes, 

/**
 * option: prepend string for attributes
 *
 * Possible values:
 * - any string (default is any string)
 */
//XML_SERIALIZER_OPTION_PREPEND_ATTRIBUTES => prependAttributes, 

/**
// * option: indent the attributes, if set to '_auto',
 * it will indent attributes so they all start at the same column
 *
 * Possible values:
 * - true
 * - false (default)
 * - '_auto'
 */
//XML_SERIALIZER_OPTION_INDENT_ATTRIBUTES => indentAttributes, 

/**
 * option: use 'simplexml' to use parent name as tagname
 * if transforming an indexed array
 *
 * Possible values:
 * - XML_SERIALIZER_MODE_DEFAULT (default)
 * - XML_SERIALIZER_MODE_SIMPLEXML
 */
//XML_SERIALIZER_OPTION_MODE => mode, 

/**
 * option: add a doctype declaration
 *
 * Possible values:
 * - true
 * - false (default)
 */
//XML_SERIALIZER_OPTION_DOCTYPE_ENABLED => addDoctype, 

/**
 * option: supply a string or an array with id and uri
 * ({@see XML_Util::getDoctypeDeclaration()}
 *
 * Possible values:
 * - string
 * - array
 */
//XML_SERIALIZER_OPTION_DOCTYPE => doctype, 

/**
 * option: name of the root tag
 *
 * Possible values:
 * - string
 * - null (default)
 */
//XML_SERIALIZER_OPTION_ROOT_NAME => rootName, 

/**
 * option: attributes of the root tag
 *
 * Possible values:
 * - array
 */
//XML_SERIALIZER_OPTION_ROOT_ATTRIBS => rootAttributes, 

/**
 * option: all values in this key will be treated as attributes
 *
 * Possible values:
 * - string
 */
//XML_SERIALIZER_OPTION_ATTRIBUTES_KEY => attributesArray, 

/**
 * option: this value will be used directly as content,
 * instead of creating a new tag, may only be used
 * in conjuction with attributesArray
 *
 * Possible values:
 * - string
 * - null (default)
 */
//XML_SERIALIZER_OPTION_CONTENT_KEY => contentName, 

/**
 * option: this value will be used in a comment, instead of creating a new tag
 *
 * Possible values:
 * - string
 * - null (default)
 */
//XML_SERIALIZER_OPTION_COMMENT_KEY => commentName, 

/**
 * option: tag names that will be changed
 *
 * Possible values:
 * - array
 */
//XML_SERIALIZER_OPTION_TAGMAP => tagMap, 

/**
 * option: function that will be applied before serializing
 *
 * Possible values:
 * - any valid PHP callback
 */
//XML_SERIALIZER_OPTION_ENCODE_FUNC => encodeFunction, 

/**
 * option: namespace to use for the document
 *
 * Possible values:
 * - string
 * - null (default)
 */
//XML_SERIALIZER_OPTION_NAMESPACE => namespace, 

/**
 * option: type of entities to replace
 *
 * Possible values:
 * - XML_SERIALIZER_ENTITIES_NONE
 * - XML_SERIALIZER_ENTITIES_XML (default)
 * - XML_SERIALIZER_ENTITIES_XML_REQUIRED
 * - XML_SERIALIZER_ENTITIES_HTML
 */
'XML_SERIALIZER_OPTION_ENTITIES' => 'none', 

/**
 * option: whether to return the result of the serialization from serialize()
 *
 * Possible values:
 * - true
 * - false (default)
 */
//XML_SERIALIZER_OPTION_RETURN_RESULT => returnResult, 

/**
 * option: whether to ignore properties that are set to null
 *
 * Possible values:
 * - true
 * - false (default)
 */
//XML_SERIALIZER_OPTION_IGNORE_NULL => ignoreNull, 

/**
 * option: whether to use cdata sections for character data
 *
 * Possible values:
 * - true
 * - false (default)
 */
//XML_SERIALIZER_OPTION_CDATA_SECTIONS => cdata, 

/**
 * option: whether a boolean FALSE value should become a string
 *
 * Possible values:
 * - true
 * - false (default)
 *
 * @since 0.20.0
 */
//XML_SERIALIZER_OPTION_FALSE_AS_STRING => falseAsString, 

/**
 * default mode
 */
//XML_SERIALIZER_MODE_DEFAULT => default, 

/**
 * SimpleXML mode
 *
 * When serializing indexed arrays, the key of the parent value is used as a tagname.
 */
'XML_SERIALIZER_MODE_SIMPLEXML' => 'simplexml', 

/**
 * error code for no serialization done
 */
//XML_SERIALIZER_ERROR_NO_SERIALIZATION', 51


/**
 * do not replace entitites
 */
'XML_SERIALIZER_ENTITIES_NONE' => 'XML_UTIL_ENTITIES_NONE'

/**
 * replace all XML entitites
 * This setting will replace <, >, ", ' and &
 */
//XML_SERIALIZER_ENTITIES_XML', XML_UTIL_ENTITIES_XML, 

/**
 * replace only required XML entitites
 * This setting will replace <, " and &
 */
//XML_SERIALIZER_ENTITIES_XML_REQUIRED', XML_UTIL_ENTITIES_XML_REQUIRED, 

/**
 * replace HTML entitites
 * @link    http://www.php.net/htmlentities
 */
//XML_SERIALIZER_ENTITIES_HTML', XML_UTIL_ENTITIES_HTML

);

