<?xml version="1.0" encoding="utf-8"?>
<!--

/*

This file is part of e911-manager

    e911-manager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

    e911-manager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
along with e911-manager.  If not, see <http://www.gnu.org/licenses/>.

Copyright © 2016 koanhead <koanhead@callpipe.com>

*/
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	  <xsl:output method="html" indent="yes" encoding="UTF-8"/>
	<xsl:template match="/root/array/">
		
			<ul>
			<xsl:for-each select="XML_Serializer_Tag">
				<li><xsl:value-of select="class"/> : <xsl:value-of select="name"/> 
					
				</li>
			</xsl:for-each>
			</ul>
	  </xsl:template>
	  
	  <xsl:template match="/root/stdClass/data">
	      <h2>List</h2>
	      <table border="1">
	        <tr>
	          <th>id</th>
	          <th>email</th>
	          <th>Address</th>
	          <th>First Name</th>
	          <th>Last Name</th>
	        </tr>
	        <xsl:for-each select="XML_Serializer_Tag">
	        <tr>
	          <td><xsl:value-of select="id"/></td>
	          <td><xsl:value-of select="email"/></td>
	          <td><xsl:value-of select="address"/></td>
	          <td><xsl:value-of select="first_name"/></td>
	          <td><xsl:value-of select="last_name"/></td>
	        </tr>
	        </xsl:for-each>
	      </table>
	  
	</xsl:template>
	
<!--	</xsl:transform>
-->
</xsl:stylesheet>
