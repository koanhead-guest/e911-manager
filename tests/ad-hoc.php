<?php
/*
 * ad-hoc.php
 * 
 * Copyright 2016 koanhead <koanhead@callpipe.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
//include('root.php');
include('/var/www/fusionpbx/app/e911-manager/root.php');
include('/var/www/fusionpbx/app/e911-manager/display-functions.php');
include('/var/www/fusionpbx/app/e911-manager/provider/teleapi/php-client/BaseTele.class.php');

include('/var/www/fusionpbx/app/e911-manager/tests/testclass.php');
$testobj = new testClass;

//print(listify($testobj->propassoc, 'farts'));
//print_r(show_params('Tele911', 'create'));
$ummat = new DOMDocument;
$content = array2node(show_params('Tele911', 'create'), 'td', $ummat);
$icky = $ummat->createElement('div', '');
//echo get_class($content);
$icky->setAttribute('id', 'fuop');
$ummat->appendChild($icky);
$icky->appendChild($content);


print($ummat->saveHTML());
?>

