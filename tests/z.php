<?php
/*
 * z.php
 * 
 * Copyright 2016 koanhead <koanhead@callpipe.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
ini_set('display_errors', 0);
ini_set('log_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL  | E_STRICT);

include('/var/www/fusionpbx/app/e911-manager/constants.php');
include('/var/www/fusionpbx/app/e911-manager/provider/teleapi/php-client/BaseTele.class.php');
include('/var/www/fusionpbx/app/e911-manager/provider/display-functions.php');

//include('/var/www/fusionpbx/app/e911-manager/provider/teleapi/Request.php');

function whatis($this_thing) {
    $basetype = gettype($this_thing);
    if( $basetype == 'object' ) {
            $output =  (get_class($this_thing));
        }
    else {
        $output = $basetype;
    }
    return $output;
}

function is_assoc($array) {
  foreach (array_keys($array) as $k => $v) {
    if ($k !== $v)
      return true;
  }
  return false;
}

$test_array = Array (0 => 'first', '1' => 2, 3 => FALSE, 'four'=> TRUE);
?>
