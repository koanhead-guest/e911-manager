<?php
/*
 * constants.php
 * 
 * Copyright 2017 koanhead <koanhead@fagioli>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

	define('APPNAME', 'e911-manager');
	
	if ( $_SERVER["DOCUMENT_ROOT"] != '/var/www/html/' ) {
		define('DOCROOT', '/var/www/fusionpbx/app/');
		
	}
		
	else {
		define('DOCROOT', $_SERVER["DOCUMENT_ROOT"]);
	}
	
	define('APP_ROOT', DOCROOT . APPNAME . "/");
	define('TELE_PATH', APP_ROOT . "provider/teleapi/");
	
	define('BASEDOC', APP_ROOT . "provider/parent-doc.html");
	//set to false to suppress debug messages
	define('DEBUG', TRUE);
