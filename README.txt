FusionPBX E911 Manager App

DESCRIPTION:


This file introduces e911-manager and describes its operation.
It is based on requirements.md, which may go away (along with this line) eventually.

This app displays a table with the following fields:
* e911 Registration (SIP account registered with a e911 outgoing DID; editable via html SELECT element)
* DID for outgoing 911 calls (editable with SELECT of DIDs for outbound e911 calls.)
* Address (editable with textarea)
* Verified (TRUE or error message should assignment or query fail)
* Routed to (null or SIP account to which incoming calls to this DID will route)

The source code refers to the data structure holding these fields as a 'binding' as in e911Binding
Fields are visible to all users, editable only by admins.

The app also provides list of available DIDs and a textarea to add new DIDs if user has appropriate permission.

When a SIP account originates a 911 call the app routes it to the assigned DID or
assigns an unused DID from its list to that account. It then gets address information about the SIP
account from the table and uses the e911 gateway provider's API to bind that information to the DID 
so that PSAP can see it.

CONFIGURATION:

The app provides an interface to introspect the provider's client library and generate a web interface from it so that the administrator
can set up new accounts, choose display columns, etc. This works as follows:

# Reads the list of subclasses from the appropriate directory
# For each subclass, gets the list of properties and that of methods
# Displays the properties in a table, 1 row per property, cells for each value of property
# Displays the methods in a list, clicking the relevant list element generates interface elements appropriate to the types of arguments the method accepts

HOW IT WORKS:

Freeswitch keeps most of its configuration in XML.
This app can also use XML along with XSD and XSLT to directly transform the XML data into interface elements.

 
Upon invocation (i.e., from autoload_configs) the app compiles a list of extensions from the relevant conf/directory/ (that is, a directory *called* "directory", containing user information) for the domain of the caller (cli or function or whatnot).
(walk the directory, look into each ext for an e911-related tagset:
    <e911 enabled=yes|no>
        <address>address</address>
        <e911DID timestamp="$time"> DID </e911DID>
    </e911>
    
    Entries without e911 tagset have not been designated as e911 numbers, either because they have not been processed yet or because we don't want them to have 911 access for some reason. These extensions will not show up in the table.
    
    Entries with enabled=no have not had 911 service added on the provider side, or have had it explicitly removed or denied, or the request failed. If the latter then the <verified> tag in e911-manager.xml should contain an error code. Parsing stops at enabled= attribute if value is "no".
    These extensions will show up in the table with a graphical representation of their status, like tr: {backround:red} or something.
    
    Entries with enabled=yes have had 911 service added on the provider side, meaning that the 'add' request has been sent and reply code 200 received.
        
        Entries with enabled=yes and with <address> tag empty show in the table, f.e. tr: {background:orange}, with the necessary input elements in the address row. Address is a complex element, so multiple input elements (i.e., a whole form) will be needed.
        
        Entries with enabled=yes and with <address> populated but <outgoingDID> empty show in the table, f.e. tr: {background:yellow} and a select element in the outgoing-DID row to select from the list of available DIDs manually. Manually-set DIDs have 'ttl="manual" ' so they do not time out.
        Entries with enabled=yes and with <address> populated, with <outgiongDID> populated but ttl=0 display the same as above. The expired DID will appear in the select element with the other available DIDs.
        
        Entries with enabled=yes and with <address> populated, with <outgiongDID> populated and ttl!=0 show in the table f.e. tr: {background:green} without any input elements.

WHEN all this BS happens: 
    on startup (script invoked from autoload_configs)
    on http request from $frontend
    
These <editme> tags will be transformed by XSLT into the relevant (select / textarea) elements in the resulting HTML.
When this process is complete the resulting XML file:
    Is transformed by the XSLT on the server side, and the resulting HTML sent when requested, OR
    Is sent along with the XSLT when requested, to be transformed on the client side
    (probably the latter)
    
    TODO: add these files to the other xml files
    along with the accompanying JS to handle the XMLhttpRequests which update the table and submit information to the app.
    TODO Query handler to receive these requests and transform them into the upstream providers' desired forms
    
    The AJAX stuff sends updates to the app on change (select) or on some other event (textarea) and updates the table in response to data from the app.
    
When new information is entered value of Verified becomes "False". Verified becomes "True" only after response received from gateway. After a specified timeout(TBD), Verified becomes "False" again.
Bindings where Verified="True" are eligible for routing. 
 in order to make the routing phase happen we need to add a module to freeswitch which manages the DIDs. It can be triggered by dialplan <condition field="destination_number" expression="^911"> and modify routing as desired.
<regex field="${emergency_call}" expression="^true$"/>


TODO:

    FILES:
        e911.xsd -> XML Schema for e911 configuration in Freeswitch (currently 'teleapi.xsd', generalize later)
        e911manager-dialplan.xml -> XML dialplan file for e911-manager
        e911.xslt -> Stylesheet for xml transforms
        e911manager.html -> generated via xslt from e911.xml, contains UI elements as spec in DESCRIPTION
        
