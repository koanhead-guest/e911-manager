<?php
/*
 * z.php
 * 
 * Copyright 2016 koanhead <koanhead@callpipe.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL  | E_STRICT);

include('/var/www/fusionpbx/app/e911-manager/constants.php');
include('/var/www/fusionpbx/app/e911-manager/provider/teleapi/php-client/BaseTele.class.php');
include('/var/www/fusionpbx/app/e911-manager/provider/display-functions.php');

//include('/var/www/fusionpbx/app/e911-manager/provider/teleapi/Request.php');

function whatis($this_thing) {
    $basetype = gettype($this_thing);
    if( $basetype == 'object' ) {
            $output =  (get_class($this_thing));
        }
    else {
        $output = $basetype;
    }
    return $output;
}

function validate_args($args, $types) {
	if (count($args) != count($types)) {
		die("need a type for each arg");
	}
	else {
		die("function validate_args is not implemented yet!");
    }
		//plop args and types into assoc array
		// for each {$args_assoc as $key) {
		// if ($key=>$type != whatis($key=>$arg)) 
		//	   die("type mismatched at idx $index")
		//   
		
}
//require_once('php-client/BaseTele.class.php');
//fix this path
//http://docs.teleapi.net/docs/phplibrary for docs on this class
?>
